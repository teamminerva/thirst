package com.example.thirst;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import android.app.Activity;
import android.database.SQLException;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

public class SecondBarlist extends Activity{
	
	LinearLayout ll;
//	 int recordNumber = 3; // this isn't even doing anything right now; it just shows all the bars
//     final CheckBox[] cbArray = new CheckBox[recordNumber];
     String bar = "";
     String barPrice = "";
	 String barLat = "";
	 String barLong =  "";
	 String barRating =  "";
	
     
     @Override
 	protected void onCreate(Bundle icicle) {
         super.onCreate(icicle);
         setContentView(R.layout.barlist);
         
     	ll = (LinearLayout)findViewById(R.id.barlistlinearlayout);
     	ll.setVerticalScrollBarEnabled(true);

         Thread t = new Thread(background, "second");
         t.start();
 	}
     
     
	// foreground thread handler 
	
	Handler handler = new Handler(){
		public void handleMessage(Message m){
			
			MyBarlist obj = (MyBarlist)m.obj;
			
			final CheckBox box = new CheckBox(getApplicationContext());
			box.setText(obj.getBar());
			box.setTextColor(Color.parseColor("#FFFFFF"));
			box.setBackgroundColor(Color.parseColor("#0000AA"));
			ll.addView(box);
			
			box.setOnClickListener(new OnClickListener() {
				public void onClick(View v){
					if (box.isChecked()){
						// box.setText("check!"); -> testing the listener. works.
						// CODE THAT ADDS BAR TO CRAWL WILL GO HERE.
					}
				}
			});
			
			box.setOnLongClickListener(new OnLongClickListener() { public boolean onLongClick(View v) { 
			
			if (box.isPressed()) {
				LayoutInflater layoutInflater = (LayoutInflater)getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);  
	            View popupView = layoutInflater.inflate(R.layout.popup, null);
	        	final PopupWindow pop = new PopupWindow(popupView, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

	        	TextView barInfo = (TextView)popupView.findViewById(R.id.textView2);
	        	
	        	String station = (String) RedSpinner.selection.getText(); // gets the station you selected.

	        	barInfo.setText(station);  // dynamically change the text here.
	        	
	        	popupView.setAlpha(1);		//we changed the min sdk in the manifest from 8 to 11 to make this not hate itself
	        	
	        	pop.showAtLocation(v, Gravity.LEFT, 30, 50);

		        	Button btnDismiss = (Button)popupView.findViewById(R.id.close);
	        	
	        	btnDismiss.setOnClickListener(new Button.OnClickListener(){

	        	     @Override
	        	     public void onClick(View v) {
	        	      pop.dismiss();
	        	     }}); 
	        	     
			}
			return false;}} 
						);
			}
			}; // foreground thread ends
			
			
	
    // ----
// background thread
    
    Runnable background = new Runnable() {
    	public void run() {
    		
    		 Looper.prepare(); // an error message told me to add this. 
    
    String URL = "jdbc:mysql://frodo.bentley.edu:3306/cs460minerva";
    String username = "cs460Minerva";
    String password = "cs460minerva";
    
    
  //  String barPrice = "";
  //  String barRating = "";
  //  String barLat = "";
  //  String barLong = "";
    
    
    try {
    	try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	} catch (SQLException e) {
		e.printStackTrace();
	}    
    try {
		 try {
			Connection con = DriverManager.getConnection(URL, username, password);
		     Statement stmt = con.createStatement();
		     String barQuery = "SELECT * FROM bar;";

		     ResultSet barResults = stmt.executeQuery(barQuery);

		    
        
        // Loop starts here...
        
        while (barResults.next())
        {
        	
        	
        	
        	bar = barResults.getString("Bar_Name");
   		  	barPrice = barResults.getString("Price");
   		  	barLat = barResults.getString("Latitude");
   		  	barLong = barResults.getString("Longitude");
   		  	barRating = barResults.getString("Rating");
   		  	Log.e(bar, barRating);

   		  
        	
        	
        	// this goes in the loop.
       	 	 MyBarlist objs = new MyBarlist(bar, barPrice, barLat, barLong, barRating);
		     Message msg = handler.obtainMessage();
		     msg.obj = objs;
		     handler.sendMessage(msg);
       	//
        	
        } // Loop ends here.
        
		 } catch (java.sql.SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        
	}finally{}
      

	
}
};	
    
	}
