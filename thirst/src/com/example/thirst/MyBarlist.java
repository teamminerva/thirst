package com.example.thirst;

public class MyBarlist {
	
	private String bar;
	private String barPrice;
	private String barLat;
	private String barLong;
	private String barRating;

	
	//constructor
	public MyBarlist (String a, String b, String c, String d, String e) {
		
		bar = a;
		barPrice = b;
		barLat = c;
		barLong = d;
		barRating = e;
	}
	
//	public MyBarlist()
//	{
//		this()
//	}
	
	
	//accessors
	public String getBar(){
		return bar;
	}
	
	public String getBarPrice(){
		return barPrice;
	}
	
	public String getBarLat(){
		return barLat;
	}
	
	public String getBarLong(){
		return barLong;
	}
	
	public String getBarRating(){
		return barRating;
	}
	
	
	//mutators
	public void setBar (String str){
		bar = str;
	}
	
	public void setBarPrice (String str){
		barPrice = str;
	}
	
	public void setBarLat (String str){
		barLat = str;
	}
	
	public void setBarLong (String str){
		barLong = str;
	}
	
	public void setBarRating (String str){
		barRating = str;
	}
	
}
