package com.example.thirstfinalscreen;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.CameraUpdateFactory;

public class FinalCrawlScreen extends Activity implements View.OnClickListener {

	// put in actual coordinates of Noir and Oberon later?
	private static final LatLng HARVARDSQUARE = new LatLng(42.3736, 71.1189);
	private static final LatLng NOIR = new LatLng(42.373123, -71.119656);
	private static final LatLng RUSSELL = new LatLng(42.373123, -71.119656);
	private static final LatLng OBERON = new LatLng(42.373123, -71.119656);
	private ListView barlistview;

	// GoogleMap mymap;

	// Below is an ArrayAdapter that fills the ListView with dummy data. Will be
	// replaced by a loop that will populate the ArrayList with the elements of
	// the bars chosen.
	// Then, the list of bars ought to be displayed by the ArrayAdapter.

	String[] barlistarray = { "Bar 1", "Bar 2", "Bar 3" };

	ArrayAdapter<String> barListAdapter = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// mymap.setMyLocationEnabled(true);

		barlistview = (ListView) findViewById(R.id.list);

		barListAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, barlistarray);
		barlistview.setAdapter(barListAdapter);

		ImageButton tbutton = (ImageButton) findViewById(R.id.tlogo);
		tbutton.setOnClickListener((OnClickListener) this);

		ImageButton uberbutton = (ImageButton) findViewById(R.id.uberlogo);
		uberbutton.setOnClickListener((OnClickListener) this);

		ImageButton stopbutton = (ImageButton) findViewById(R.id.stopsign);
		stopbutton.setOnClickListener((OnClickListener) this);
	}

	public void onClick(View v) {
		switch (v.getId()) {

		// Takes user to the closest MBTA stop.
		case R.id.tlogo:

			break;
		// Brings user to Uber application.
		case R.id.uberlogo:
			openApp(this, "com.ubercab");
			break;
		// Exits the application.
		case R.id.stopsign:
			finish();
			Toast.makeText(getApplicationContext(), "Closing the app.",
					Toast.LENGTH_LONG).show();
			break;

		}

	}

	public void onListItemClick(ListView l, View view, final int position,
			long id) {

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public static boolean openApp(Context context, String packageName) {
		PackageManager manager = context.getPackageManager();
		Intent i = manager.getLaunchIntentForPackage("com.ubercab");
		if (i == null) {
			return false;
			// throw new PackageManager.NameNotFoundException();
		}
		i.addCategory(Intent.CATEGORY_LAUNCHER);
		context.startActivity(i);
		return true;
	}

}
