package com.example.thirst;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;


public class RedSpinner extends Activity implements OnItemSelectedListener {


	public static TextView selection;
	String[] items = { "Select a station", "Alewife", "Davis", "Porter",
			"Harvard",
			"Central",
			"Kendall",
			"Charles-MGH",
			"Park Street",
			"Downtown Crossing",
			"South Station",
			"Broadway",
			"Andrew",
			"JFK-UMass",
			"North Quincy",
			"Wollaston",
			"Quincy Center",
			"Quincy Adams",
			"Braintree",
			"Savin Hill",
			"Fields Corner",
			"Shawmut",
			"Ashmont"
 };

	
	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		setContentView(R.layout.spinner);
		selection = (TextView) findViewById(R.id.selection);

		Spinner spin = (Spinner) findViewById(R.id.spinner);
		spin.setOnItemSelectedListener(this);   //set listener

		ArrayAdapter<String> aa = new ArrayAdapter<String>(
				this,
				android.R.layout.simple_spinner_item,  //Android supplied Spinner item format
				items);

		//set resource to use for drop down view, Android supplied format
		aa.setDropDownViewResource(
		   android.R.layout.simple_spinner_dropdown_item);  
		spin.setAdapter(aa);  //connect ArrayAdapter to <Spinner>
		
		
	}

	//listener methods for callbacks 
	public void onItemSelected(AdapterView<?> parent, View v, int position,
			long id) {
		selection.setText(items[position]);
    	
		if (selection.getText() != "Select a station"){
		Intent i1 = new Intent(this, SecondBarlist.class);
        startActivity(i1);
		}
		
	}

	public void onNothingSelected(AdapterView<?> parent) {
		selection.setText("");
	}
	
	
	
}//class
